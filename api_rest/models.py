from django.db import models


class DocumentFirma(models.Model):
    id = models.AutoField(primary_key=True)
    date_create = models.DateTimeField(auto_now_add=True)
    date_edit = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Doumentos"
        ordering = ['id']

    def _str_(self):
        return self.id



class sheetFirma(models.Model):
    document_id = models.ForeignKey(DocumentFirma, on_delete=models.CASCADE,related_name='id_document')
    id = models.AutoField(primary_key=True, blank=False)
    date_create = models.DateTimeField(auto_now_add=True)
    date_edit = models.DateTimeField(auto_now=True)


    def _str_(self):
        return self.id
        #return '{}:{}'.format(self.document_id.id,self.id)

    class Meta:
        ordering = ['id']
        verbose_name_plural = "Hojas"







class firmas(models.Model):
    sheet_id = models.ForeignKey(sheetFirma, on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True, blank=False)
    date_create = models.DateTimeField(auto_now_add=True)
    date_edit = models.DateTimeField(auto_now=True)


    class Meta:
        ordering = ['id']
        verbose_name_plural = "Firmas"


    def _str_(self):
        return self.id
        #return '{}'.format(self.id)
