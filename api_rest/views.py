from django.shortcuts import render
from rest_framework import generics


from .serializers import *
from .models import *


class documentList(generics.ListCreateAPIView):
    serializer_class = documentSerializer
    queryset = DocumentFirma.objects.all()

class documentSave(generics.RetrieveDestroyAPIView):
    serializer_class = documentSerializer
    queryset = DocumentFirma.objects.all()



