from rest_framework import serializers
from .models import *


class documentSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentFirma
        fields = '__all__'




class SheetSerializer(serializers.ModelSerializer):

    class Meta:
        model = sheetFirma
        fields = '__all__'


class signatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = firmas
        fields = '__all__'
