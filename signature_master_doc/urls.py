
from django.contrib import admin
from django.urls import path
from api_rest.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('documentList/', documentList.as_view(), name='listado_Documentos'),
    path('', documentList.as_view(), name='listado_Documentos'),
    #path('document/', documentSave.as_view(), name= 'Detalle_Documento'),
    #path('document/<int:pk>/sheet/', documentListSheet.as_view(), name='List_Detalle_Hojas'),
]
